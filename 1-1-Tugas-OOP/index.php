<?php 

trait Hewan {
    public $nama = "";
    public $darah = 50;
    public $jumlahKaki = "";
    public $keahlian = "";

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class Fight {
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
    }
    public function diserang($hewan)
    {
        echo "{$this->nama} sedang di serang {$hewan->nama}";
        $this->darah = $this->darah - $hewan->attackPower / $this->defencePower;
    }

    protected function getInfo() {

        echo "<br>";
        echo "Nama :{$this->nama} ";
        echo "<br>";
        echo "jumlah kaki :{$this->jumlahKaki} ";
        echo "<br>";
        echo "Keahlian :{$this->keahlian} ";
        echo "<br>";
        echo "Darah :{$this->darah} ";
        echo "<br>";
        echo "attackPower :{$this->attackPower} ";
        echo "<br>";
        echo "defencePower :{$this->defencePower} ";


    }          

}



class Elang extends Fight{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
        
    }
    public function getInfoHewan(){
        $this->getInfo();
    }
}

class Harimau extends Fight
{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7 ;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        $this->getInfo();
    }
}

class baris {
    public static function buatBaris(){
        echo "<br>";
        echo "===================";
        echo "<br>";

    }

}


$elang = new Elang("Elang");
$elang->getInfoHewan();

baris::buatBaris();

$harimau = new Harimau("Harimau");
$harimau->getInfoHewan();

baris::buatBaris();

$elang->diserang($harimau);

$harimau->getInfoHewan();

baris::buatBaris();


$elang->getInfoHewan();
