<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Traints\UserUuid;

class Roles extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $incrementing = false;

   use UserUuid;
}
